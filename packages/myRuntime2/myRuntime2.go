package myRuntime2

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
)

func myRuntime2() {
	mem := &runtime.MemStats{}
	runtime.ReadMemStats(mem)
	fmt.Println(mem)
	c := int32(0)
	n := 200

	wg := sync.WaitGroup{}
	wg.Add(n)
	for i := 0; i < n; i++ {
		go func(i int) {
			atomic.AddInt32(&c, 1)
			wg.Done()
		}(i)
	}
	wg.Wait()
	fmt.Println(c)
}
