package myRuntime1

import (
	"fmt"
	"runtime"
	"time"
)

func sysCounter() {
	mem := &runtime.MemStats{}
	runtime.ReadMemStats(mem)
	fmt.Println("Система:", runtime.GOOS, runtime.GOARCH, "\n",
		"Версия Go:", runtime.Version(), "\n",
		"Корень", runtime.GOROOT(), "\n",
		"CPU:", runtime.NumCPU(), "\n",
		"CGO:", runtime.NumCgoCall(), "\n",
		"Горутин:", runtime.NumGoroutine(), "\n",
		"Реально используемая память:", mem.Alloc, "\n",
		"Cовокупное количество байтов, выделенных для объектов кучи:", mem.TotalAlloc, "\n",
		"Память полученная от ОС:", mem.Sys, "\n",
		"Количество поисков указателей:", mem.Lookups, "\n",
		"Cовокупное количество выделенных объектов кучи:", mem.Mallocs, "\n",
		"Совокупное количество освобожденных объектов кучи:", mem.Frees, "\n",
		"Байты выделенных объектов кучи", mem.HeapAlloc, "\n",
		"Байты памяти кучи, полученные от ОС", mem.HeapSys, "\n",
		"Количество байтов в неиспользуемых диапазонах", mem.HeapIdle, "\n",
		"Количество байтов в диапазонах", mem.HeapInuse, "\n",
		"Байты физической памяти, возвращаемые ОС", mem.HeapReleased, "\n",
		"Количество выделенных объектов кучи", mem.HeapObjects, "\n",
		"Байты в интервале стека", mem.StackInuse, "\n",
		"Байты памяти стека, полученные от ОС:", mem.StackSys, "\n",
		"Байты выделенных структур mspan:", mem.MSpanInuse, "\n",
		"Байты памяти, полученные от ОС для mspan:", mem.MSpanSys, "\n",
		"Байты выделенных структур mcache:", mem.MCacheInuse, "\n",
		"Байты памяти, полученные от ОС для структуры mcache:", mem.MCacheSys, "\n",
		"Байты памяти в хэш-таблицах корзины профилирования:", mem.BuckHashSys, "\n",
		"Байты памяти в метаданных сборки мусора:", mem.GCSys, "\n",
		"Байты памяти в разной куче распределения времени выполнения:", mem.OtherSys, "\n",
		"Целевой размер кучи следующего цикла сборки мусора:", mem.NextGC, "\n",
		"Время завершения последней сборки мусора:", mem.LastGC, "\n",
		"Байты памяти стека, полученные от ОС:", mem.PauseTotalNs, "\n",
		"Циклический буфер последней остановки сборки мусора:", mem.PauseNs, "\n",
		"циклический буфер последнего времени окончания паузы сборщика мусора:", mem.PauseEnd, "\n",
		"Количество завершенных циклов сборки мусора:", mem.NumGC, "\n",
		"Количество циклов сборки мусора, принудительно выполненных приложение, вызывающее функцию GC:", mem.NumForcedGC, "\n",
		"Процессорное время, используемое сборщиком мусора с момента запуска программы:", mem.GCCPUFraction, "\n",
		"GC включен:", mem.EnableGC, "\n",
		"DebugGC включен:", mem.DebugGC,
	)
}

func myRuntime1() {
	start := time.Now()
	for i := 0; i < 10; i++ {
		go sysCounter()

		time.Sleep(time.Second)
	}
	duration := time.Since(start)
	fmt.Println("Время работы функции:", duration)
}
