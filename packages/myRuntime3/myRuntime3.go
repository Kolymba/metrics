package myRuntime3

import (
	"fmt"
	"runtime"
	"time"
)

//
func timeElapsed(what string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("%s исполнялся %v\n", what, time.Since(start))
	}
}
func MyRuntime3() {
	defer timeElapsed("main")()

	// Начальное использование памяти
	fmt.Printf("Без нагрузки:  ")
	PrintMemUsage()

	var overall [][]int
	for i := 0; i < 4; i++ {

		// Заполняем память нулями циклами
		a := make([]int, 0, 999999)
		overall = append(overall, a)

		// Вывод в цикле
		fmt.Printf("В цикле: (%v)", len(overall))
		PrintMemUsage()
		time.Sleep(time.Second)
	}

	// Очищаем память
	overall = nil
	// Запускаем сборку мусора
	runtime.GC()
	fmt.Printf("В цикле:(%v) ", len(overall))
	PrintMemUsage()
}

func PrintMemUsage() {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	fmt.Printf("Текущая = %v Kb    ", bToKb(m.Alloc))
	fmt.Printf("\tОбщая = %v Kb    ", bToKb(m.TotalAlloc))
	fmt.Printf("\tиз ОС = %v Kb    ", bToKb(m.Sys))
	fmt.Printf("\tСборка мусора = %v\n     ", m.NumGC)

}

func bToKb(b uint64) uint64 {
	return b / 1024
}
