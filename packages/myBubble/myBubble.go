package myBubble

import "fmt"

func MyBubble() {

	arr := []int{11, 300, 9, 932, 784, 28}
	fmt.Println("До:", arr)
	for i := 0; i < len(arr); i++ {
		for j := len(arr) - 1; j > i; j-- {
			if arr[j-1] > arr[j] {
				arr[j-1], arr[j] = arr[j], arr[j-1]
				fmt.Println("Во время:", arr)
			}
		}
	}
	fmt.Println("После:", arr)
}
