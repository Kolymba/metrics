package myMetrica

import (
	"fmt"
	"runtime/metrics"
)

func myMetrica() {
	// Получаем описания для всех поддерживаемых показателей.
	descs := metrics.All()

	// Создаем образец для каждой метрики.
	samples := make([]metrics.Sample, len(descs))
	for i := range samples {
		samples[i].Name = descs[i].Name
	}

	// Выборка показателей. Если можете, используйте фрагмент образцов повторно!
	metrics.Read(samples)

	// Перебираем все результаты.
	for _, sample := range samples {
		// Извлекаем имя и значение.
		name, value := sample.Name, sample.Value

		// Обрабатываем каждый образец.
		switch value.Kind() {
		case metrics.KindUint64:
			fmt.Printf("%s: %d\n", name, value.Uint64())
		case metrics.KindFloat64:
			fmt.Printf("%s: %f\n", name, value.Float64())
		case metrics.KindFloat64Histogram:
			// Гистограмма может быть довольно большой, поэтому давайте просто вытащим
			// грубая оценка медианы для этого примера.
			fmt.Printf("%s: %f\n", name, medianBucket(value.Float64Histogram()))
		case metrics.KindBad:
			// Этого никогда не должно происходить, потому что поддерживаются все показатели
			// по построению.
			panic("bug in runtime/metrics package!")
		default:
			// Это может произойти при добавлении новых показателей.
			//
			// Самое безопасное, что здесь можно сделать, это просто зарегистрировать его где-нибудь
			// как предмет для изучения, но пока не обращайте на это внимания.
			// В худшем случае вы можете временно пропустить новую метрику.
			fmt.Printf("%s: unexpected metric Kind: %v\n", name, value.Kind())
		}
	}
}

func medianBucket(h *metrics.Float64Histogram) float64 {
	total := uint64(0)
	for _, count := range h.Counts {
		total += count
	}
	thresh := total / 2
	total = 0
	for i, count := range h.Counts {
		total += count
		if total >= thresh {
			return h.Buckets[i]
		}
	}
	panic("should not happen")
}
